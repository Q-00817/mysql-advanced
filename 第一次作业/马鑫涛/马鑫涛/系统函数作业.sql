-- 1. 随机生成短信验证码（6位数字）
SELECT RPAD(ROUND(RAND()*1000000,0),6,0);

-- 2. 统计每个用户的所有卡余额之和，并四舍五入，保留一位小数
SELECT emp.RealName,emp.AccountId,ROUND(emp.`总额`,1) 总额 FROM
(
	SELECT a.RealName,b.AccountId,sum(CardMoney) 总额 FROM bankcard b
INNER JOIN accountinfo a on a.AccountId = b.AccountId
GROUP BY AccountId
)emp;

-- 3.查询用户表中名字部分为中文的所有用户记录（即必须含有中文，但不能全部是中文，比如‘刘备a1’，但不能是'刘备'）
SELECT  * from accountinfo 
where LENGTH(RealName)  != CHAR_LENGTH(RealName) and LENGTH(RealName)%CHAR_LENGTH(RealName) !=0;

-- 4.找出所有含有非法字符（'笨蛋'，'傻瓜','死'）的记录，并这些非法字符，比如 '死张三是傻瓜' 变成 '*张三是**'（几个字符，就替换为几个*）
SELECT  REPLACE(REPLACE (REPLACE(RealName,'死','*'),'笨蛋','**'),'傻瓜','**') FROM accountinfo;

-- 5.查找账号表中相同月份开户的用户
#5.1
SELECT INSERT(INSERT(CardTime,1,5,''),3,-1,'') as 开户月份 ,COUNT(*) from bankcard
GROUP BY 开户月份;
#5.2
SELECT  a.RealName,INSERT(INSERT(CardTime,1,5,''),3,-1,'') as 开户月份 from bankcard b
INNER JOIN accountinfo a on a.AccountId = b.AccountId;
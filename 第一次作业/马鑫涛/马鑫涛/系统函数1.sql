##################数值函数


SELECT CardMoney 金额, CEIL(CardMoney) 向上取整1,CEILING(CardMoney)向上取整2,
FLOOR(CardMoney)向下取整, ROUND(CardMoney) 
四舍五入1,ROUND(CardMoney,1) 四舍五入2 FROM `bankcard` ;



## 取得1-10之间的随机数
SELECT CEIL(RAND()*10);



##################字符串函数
#######  长度
#LENGTH:  字节长度(数字，字母，符号占用一个字节，一个汉字占3个字节--utf8)
#CHAR_LENGTH 字符长度
SELECT RealName,LENGTH(RealName) 字节长度,CHAR_LENGTH(RealName) 字符长度1,CHARACTER_LENGTH(RealName) as 字符长度2 from accountinfo;


##找出含有中文的
SELECT  * from accountinfo 
where LENGTH(RealName)  != CHAR_LENGTH(RealName) ;

##找出名字全部是汉字的记录
SELECT  * from accountinfo 
where LENGTH(RealName)  != CHAR_LENGTH(RealName) and LENGTH(RealName)%CHAR_LENGTH(RealName) =0;

SELECT  * from accountinfo 
where LENGTH(RealName)  =3* CHAR_LENGTH(RealName);




#######  替换 
##INSERT(str,pos,len,newstr)  指定位置、指定长度替换
##REPLACE(str,from_str,to_str) 模糊查找替换

SELECT RealName,INSERT(RealName,2,1,'三丰'),REPLACE(RealName,'飞','三丰')
from accountinfo
where AccountId=4
;
##获取日期中的年月
select CardTime,INSERT(CardTime,5,-1,'') as 年,INSERT(INSERT(CardTime,1,5,''),3,-1,'') as 月 from bankcard;


####截取 LEFT(str,len) RIGHT(str,len) substr

select CardTime,LEFT(CardTime,4)年,right(CardTime,8),SUBSTR(CardTime,6,2),SUBSTR(CardTime,6),SUBSTRing(CardTime,6,2),SUBSTRing(CardTime,6) from bankcard
;




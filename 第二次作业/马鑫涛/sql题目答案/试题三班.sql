
#吴孝涵

-- 1.查询"01"课程比"02"课程成绩高的 学生的信息 及课程分数

SELECT * from sc A ,sc B, student C
WHERE A.sid=B.sid and A.sid =C.sid and A.cid=01 and B.cid=02 and A.score>B.score



-- 1.1 查询存在" 01 "课程但可能不存在" 02 "课程的情况(不存在时显示为 null )

SELECT * FROM sc A
LEFT join sc B on A.sid = B.sid and B.cid=02
WHERE A.cid='01' 



-- 1.2 查询同时存在01和02课程的情况

SELECT * from sc A ,sc B
WHERE A.sid=B.sid and A.cid=01 and B.cid=02



-- 1.3 查询选择了02课程但没有01课程的情况
SELECT
	* 
FROM
	sc 
WHERE
	sid  not IN ( SELECT sid FROM sc WHERE cid = 01 ) and cid =02
	
SELECT sid from sc WHERE cid =01



-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩

select A.sid,COUNT(A.sid),SUM(A.score),(SUM(A.score)/COUNT(A.sid)),B.* from sc A
INNER join student B on A.sid=B.sid
GROUP BY A.sid having (SUM(score)/COUNT(A.sid))>60



-- 3.查询在 SC 表存在成绩的学生信息
select * from student where sid in
(
	-- 有成绩的学生ID
	SELECT DISTINCT sid  from sc
)
;

-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的成绩总和
SELECT sid,sname,(
	SELECT count(*) from sc where sid=student.sid
)选课总数,
(
	SELECT IFNULL(sum(score),0) from sc where sid=student.sid
)成绩总和
from student


select student.sid,student.sname,IFNULL(temp.选课总数,0)选课总数,IFNULL(temp.成绩总和,0)成绩总和 from student
LEFT join 
(
	SELECT sid,count(*)选课总数,sum(score)成绩总和 from sc 
	GROUP BY sid
)temp on temp.sid=student.sid



-- 5.查询「李」姓老师的数量

SELECT COUNT(*) from teacher WHERE tname LIKE '李%'


-- 6.查询学过「张三」老师授课的同学的信息

SELECT * from student where sid in 
(
	SELECT sid from sc where cid=
	(
		select cid from course where tid=
		(
			SELECT tid from teacher where tname='张三'
		)
	)
)
;

-- 7.查询没有学全所有课程的同学的信息
##1. NOT IN
select * from student WHERE sid NOT in
(
	select sid  from sc
	GROUP BY sid
	HAVING count(*) >= 
	(
		SELECT COUNT(*) FROM course
	)
)
##2. IN JOIN


#殷晨旭




-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息

select cid from sc
where sid = '01'

select * from student s
inner join sc on s.sid = sc.sid
where cid in
(
	select cid from sc
	where sid = '01'
)
and
s.sid <> '01'
group by s.sid

-- 9.查询和" 01 "号的同学学习的课程完全相同的其他同学的信息

select cid from sc
where sid = '01'

select count(*) from sc
where sid = 01
group by sid

select sid from sc
left join 
(
	select cid from sc
	where sid = '01'
) a
on sc.cid = a.cid
where a.cid is null
group by sc.sid

select * from student s
inner join sc on s.sid = sc.sid
where cid IN
(
	select cid from sc
	where sid = '01'
)
and s.sid not in
(
	select sid from sc
	left join 
	(
		select cid from sc
		where sid = '01'
	) a
	on sc.cid = a.cid
	where a.cid is null
	group by sc.sid
)
group by s.sid
having count(*) =
(
	select count(*) from sc
	where sid = 01
	group by sid
)

-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名

select cid from teacher t
inner join course c on t.tid = c.tid
where tname = '张三'

select * from student s
inner join sc on s.sid = sc.sid
where s.sid not in
(
	select s.sid from student s
	inner join sc on s.sid = sc.sid
	where cid = 02
	group by s.sid
)
group by s.sid


-- 11.查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩

select sid , avg(score) 平均成绩 from sc
group by sid

select * from student s
inner join sc on s.sid = sc.sid
inner join 
(
	select sid , avg(score) 平均成绩 from sc
	group by sid
) a
on sc.sid = a.sid
where score < 60
group by s.sid
having count(*) >= 2

-- 12.检索" 01 "课程分数小于 60，按分数降序排列的学生信息
##1.
SELECT student.* from student
inner join 
(
	SELECT * from sc
	where cid='01' and score<60
)temp on temp.sid = student.sid
ORDER BY score desc

##2 无法排序
-- SELECT student.* from student where sid in
-- (
-- 	SELECT sid from sc
-- 	where cid='01' and score<60
-- )


-- 13.按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩

#学生的平均成绩
SELECT b.*,a.平均成绩 from 
(
	SELECT sid,ROUND(AVG(score),2)平均成绩 from sc
	GROUP BY sid
)a
right join 
(
	SELECT s.*,sc1.score 语文成绩,sc2.score 数学成绩,sc3.score 英语成绩 from student s
	LEFT join sc sc1 on sc1.sid=s.sid and sc1.cid='01'#语文
	LEFT join sc sc2 on sc2.sid=s.sid and sc2.cid='02'#数学
	LEFT join sc sc3 on sc3.sid=s.sid and sc3.cid='03'#英语
)b
on a.sid=b.sid
ORDER BY a.平均成绩 desc








-- 14.查询各科成绩最高分、最低分和平均分,以如下形式显示：
-- 课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
-- 及格为>=60，中等为：70-80，优良为：[80-90)，优秀为：>=90
-- 要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序
#1.
SELECT *,(
	SELECT max(score) from sc where sc.cid=course.cid
)最高分,
(
	SELECT min(score) from sc where sc.cid=course.cid
)最低分,
(
	SELECT ROUND(avg(score),2) from sc where sc.cid=course.cid
)平均分,
(
	#及格人数
	(SELECT count(score) from sc where sc.cid=course.cid and score>=60)
		/
	#总人数
	(SELECT count(score) from sc where sc.cid=course.cid) 
)及格率,
(
	CONCAT(ROUND(100*(SELECT count(score) from sc where sc.cid=course.cid and score>=70 && score<80)
		/
	(SELECT count(score) from sc where sc.cid=course.cid) ,2),'%')
)中等率,
(
	(SELECT count(score) from sc where sc.cid=course.cid and score>=80 && score<90)
		/
	(SELECT count(score) from sc where sc.cid=course.cid) 
)优良率,
(
	(SELECT count(score) from sc where sc.cid=course.cid and score>=90)
		/
	(SELECT count(score) from sc where sc.cid=course.cid) 
)优秀率
from course
;

#2.
select cid,max(score) 最高分,min(score) 最低分,avg(score) 平均分,sum(IF(score>=60,1,0))/count(*) 及格率,
sum(IF(score>=70 && score<80,1,0))/count(*) 中等率,
sum(IF(score>=80 && score<90,1,0))/count(*) 优良率,
sum(IF(score>=90,1,0))/count(*) 优秀率
from sc
GROUP BY cid 
;



-- 15.按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺
#1.
		SELECT sc1.sid,sc1.cid ,sc1.score ,count(sc2.score)+1 排名 from  sc sc1
		left join sc sc2 on sc1.cid =sc2.cid and sc1.sid!=sc2.sid and sc1.score<sc2.score
		GROUP BY sc1.sid,sc1.cid,sc1.score 
		order by sc1.cid asc,sc1.score desc
		;
#2.
select *,(
	select count(*)+1 from sc temp where temp.cid=sc.cid and temp.score>sc.score
)排名
from sc
order by sc.cid asc,排名 asc
;



-- 15.1 按各科成绩进行行排序，并显示排名， Score 重复时合并名次

select *,
(
	select count(DISTINCT temp.score)+1 from sc temp where temp.cid=sc.cid and temp.score>sc.score
)排名
from sc
order by sc.cid asc,排名 asc
;

-- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺

-- SELECT sid,sum(score)总成绩 from sc
-- GROUP BY sid
-- 
-- t

select *,
(
	select count(temp.总成绩)+1 from 
	(
		SELECT sid,sum(score)总成绩 from sc
		GROUP BY sid
	)temp where temp.总成绩>t.总成绩
)排名
from 
(
	SELECT sid,sum(score)总成绩 from sc
	GROUP BY sid
)t
order by 排名 asc
;



#陈宇翔






-- 17. 统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比


select cid,
sum(IF(score>=85,1,0))/count(*) `[100-85]`,
sum(IF(score>=70 && score<85,1,0))/count(*) `[85-70]`,
sum(IF(score>=60 && score<70,1,0))/count(*) `[70-60]`,
sum(IF(score<60,1,0))/count(*) `[60-0]`
from sc
GROUP BY cid 
;







-- 18.查询各科成绩前三名的记录
SELECT * from 
(
	select *,
	(
		select count(DISTINCT temp.score)+1 from sc temp where temp.cid=sc.cid and temp.score>sc.score
	)排名
	from sc
	order by sc.cid asc,排名 asc
)temp where 排名<=3



-- 19.查询每门课程被选修的学生数
select *,
(
	select count(*) from sc where cid=course.cid
)学生数
from course
	


-- 20.查询出只选修两门课程的学生学号和姓名
select * from student where sid in 
(
	SELECT sid from sc
	GROUP BY sid
	HAVING count(*)=2
)



-- 21. 查询男生、女生人数
SELECT 
(
	select count(*) from student where ssex='男'
)男生人数,
(
	select count(*) from student where ssex='女'
)女生人数

-- 22. 查询名字中含有「风」字的学生信息


-- 23查询同名同姓学生名单，并统计同名人数

SELECT sname,count(*)人数 from student
GROUP BY sname
HAVING COUNT(*)>1





-- 25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列

	select cid,avg(score)平均成绩 from sc 
	GROUP BY cid
	order by 平均成绩 desc,cid asc


-- 26.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩
  SELECT student.sid,student.sname,temp.平均成绩 from student 
	inner join (
		select sid,avg(score)平均成绩 from sc 
		GROUP BY sid
		HAVING avg(score)>=85
	)temp on temp.sid=student.sid






#宋嘉伟




-- 27.查询课程名称为「数学」，且分数低于 60 的学生姓名和分数

select * from student
inner join 
(
select * from sc 
WHERE cid = 
(
select cid from course where cname = '数学'
)
and score < 60
) as temp on
temp.sid = student.sid

-- 28. 查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）

select student.sname,temp.score as 语文,temp1.score as 数学,temp2.score as 英语 from student
LEFT join 
(
select * from sc 
where cid = '01' 
) as temp
on temp.sid = student.sid 
LEFT join 
(
select * from sc 
where cid = '02' 
) as temp1
on temp1.sid = student.sid 
LEFT join 
(
select * from sc 
where cid = '03' 
) as temp2
on temp2.sid = student.sid 


-- 29.查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数

select sname,sc.*,cname from sc 
inner join course
on course.cid = sc.cid 
inner join student
on sc.sid = student.sid
where score > 70

select * from sc where score > 70


-- 30.查询不及格的课程
select * from course where cid in
(
	select DISTINCT cid from sc
	where score<60
)


-- 31.查询课程编号为 01 且课程成绩在 80 分以上的学生的学号和姓名

select * from student 
inner join 
(
select * from sc where cid = '01' and score >= 80
) as temp on temp.sid = student.sid
-- 32.求每门课程的学生人数

select cname,temp.每门课程的学生人数 from course
inner JOIN
(
select cid,count(*) as 每门课程的学生人数 from sc
group by cid
) as temp on temp.cid = course.cid

-- 33.成绩不重复，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩



SELECT student.*,temp.score from student 
inner join 
(
	select * from sc 
	where cid=
	(
		select cid from course where tid=
		(
			select tid from teacher where tname='张三'
		)
	)
	order by score desc
	LIMIT 1
)temp on temp.sid = student.sid


-- 34.成绩有重复的情况下，查询选修「张三」老师所授课程的学生中，成绩最高的学生

SELECT student.*,temp.score from student 
inner join 
(
		select * from sc 
		where cid=
		(
			select cid from course where tid=
			(
				select tid from teacher where tname='张三'
			)
		) and score>=
		(
				select max(score) from sc 
				where cid=
				(
					select cid from course where tid=
					(
						select tid from teacher where tname='张三'
					)
				)
		)
)temp on temp.sid = student.sid

-- 35.查询不同课程成绩相同的学生的学生编号、课程编号、学生成绩
select  sc1.sid,sc1.cid acid,sc2.cid bcid,sc1.score
from sc sc1
inner join sc sc2 on sc1.sid=sc2.sid and sc1.cid<>sc2.cid and sc1.score=sc2.score
where sc1.cid>sc2.cid





-- 陈思锐

-- 36. 查询每门成绩最好的前两名

############################################################

select * from 
(
	SELECT sc.*,IFNULL(t.名次,1) 名次 FROM sc 
	left join 
	(
		SELECT sc1.sid,sc1.cid ,sc1.score ,count(sc1.score)+1 名次 from  sc sc1
		inner join sc sc2 on sc1.cid =sc2.cid and sc1.sid!=sc2.sid and sc1.score<sc2.score
		GROUP BY sc1.sid,sc1.cid,sc1.score 
		order by sc1.score desc
	)
	t
	on t.sid=sc.sid and t.cid=sc.cid	order by sc.cid asc, 名次 asc
)temp
where 名次<=2


#############################################################

set names utf8;

select
	b.cid,b.sid,b.score,(count(a.score)) `rank` -- 统计a表成绩比b表成绩高的人数，结果+1就是排名
from
	sc a
right join
	sc b 
on
	a.cid = b.cid and a.sid <> b.sid and a.score > b.score -- 自连接，连接条件：课程id相等且学生id不想等，最后筛选出a表成绩比b表成绩高的列
GROUP BY
	b.cid, b.sid,b.score
having
	`rank` in (1,2) 
order by
	b.cid,b.sid

-- 1
-- 成绩排名表
select
	b.cid,b.sid,b.score,(count(a.score) + 1) `rank` -- 统计a表成绩比b表成绩高的人数，结果+1就是排名
from
	sc a
right join
	sc b 
on
	a.cid = b.cid and a.sid <> b.sid and a.score > b.score -- 自连接，连接条件：课程id相等且学生id不想等，最后筛选出a表成绩比b表成绩高的列
GROUP BY
	b.cid, b.sid,b.score
having
	`rank` in (1,2) 
order by
	b.cid,`rank`;

-- 结果表
select
	a.sname,b.*,c.cname
from
	student a
join
	(
		select
			b.cid,b.sid,b.score,(count(a.score) + 1) `rank`
		from
			sc a
		right join
			sc b
		on
			a.cid = b.cid and a.sid <> b.sid and a.score > b.score
		GROUP BY
			b.cid, b.sid,b.score
		having
			`rank` in (1,2)
	) b
on
	a.sid = b.sid
join
	course c
on
	b.cid = c.cid
order by
	b.cid,b.`rank`;
	
-- 2
-- 成绩排名表
select
	a.*,
	if(@cid = a.cid,@rownum:=@rownum+1,@rownum:=1) `rank`, -- 检查：当本行班级id与变量中保存的班级id相等时保持自增，不相等时重新从1开始计算
	@cid:=a.cid
from
	(select * from sc order by cid, score desc) a ,
	(select @rownum := 0,@cid:='') b; -- 设置变量
	

	
	
-- 37. 统计每门课程的学生选修人数（超过 5 人的课程才统计）
select * from 
(
	select *,
	(
		select count(*) from sc where sc.cid=course.cid
	)人数
	from course
)temp where temp.人数>5




-- 38.检索至少选修两门课程的学生学号
SELECT sid from sc
GROUP BY sid
HAVING count(*)>=2



-- 39.查询选修了全部课程的学生信息

	SELECT sid from sc
	GROUP BY sid
	HAVING count(*)>=
	(
		select count(*) from course
	)
	
-- 24.查询 1990 年出生的学生名单
#1
select * from student where sage>='1990'and sage<'1991';
#2
select * from student where YEAR(sage)='1990';
#
select * from student where sage like '1990%';

-- 40.查询各学生的年龄，只按年份来算 虚岁
SELECT *,
(
	YEAR(NOW())-YEAR(sage)
)年龄
from student
;

-- 41. 按照出生日期来算，当前月日 < 出生年月的月日则，年龄减一 周岁
#1
	SELECT *,
	(
		CASE 
			WHEN MONTH(NOW())>MONTH(sage) THEN
				YEAR(NOW())-YEAR(sage)
			WHEN MONTH(NOW())=MONTH(sage) THEN
				CASE 
					WHEN DAYOFMONTH(NOW())>=DAYOFMONTH(sage) THEN
						YEAR(NOW())-YEAR(sage)
					ELSE
						YEAR(NOW())-YEAR(sage)-1
				END 
			ELSE
				YEAR(NOW())-YEAR(sage)-1
		END 

	)周岁
	from student;



-- 	if (当前月>出生月)
-- 	{
-- 		YEAR(NOW())-YEAR(sage)
-- 	}
-- 	else if (当前月=出生月)
-- 	{
-- 		if(当前日>=出生日){
-- 			YEAR(NOW())-YEAR(sage)
-- 		}else{
-- 			YEAR(NOW())-YEAR(sage)-1
-- 		}
-- 	}else//当前月<出生月
-- 	{
-- 		YEAR(NOW())-YEAR(sage)-1
-- 	}
-- 
#2
select * ,TIMESTAMPDIFF(YEAR,sage,NOW())周岁 from student;




-- 42.查询本周过生日的学生

-- select WEEKOFYEAR(NOW()),WEEKOFYEAR('2021-09-25') 
-- #364/7 52.22343423   366
-- 
-- select WEEKOFYEAR('1990-01-20'), WEEKOFYEAR('1991-01-20'), WEEKOFYEAR('1992-01-20'), WEEKOFYEAR('1993-01-20')
-- 
-- 2021-01-20
select * from student where WEEKOFYEAR(DATE_ADD(sage,INTERVAL (YEAR(NOW())-YEAR(sage)) YEAR))=WEEKOFYEAR(NOW())

-- 43. 查询下周过生日的学生

select * from student where WEEKOFYEAR(DATE_ADD(sage,INTERVAL (YEAR(NOW())-YEAR(sage)) YEAR))=WEEKOFYEAR(NOW())+1

-- 44.查询本月过生日的学生

select * from student where MONTH(NOW())=MONTH(sage);


-- 45.查询下月过生日的学生
select * from student where (MONTH(NOW())+1)%12=MONTH(sage)%12;



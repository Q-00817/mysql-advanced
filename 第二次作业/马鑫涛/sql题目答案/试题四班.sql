#黄基洪
-- 1.查询"01"课程比"02"课程成绩高的学生的信息及课程分数
		

-- 1.1 查询存在" 01 "课程但可能不存在" 02 "课程的情况(不存在时显示为 null )
	SELECT * from 
	(	
		SELECT *,
		(
			SELECT score from sc where cid='01' and sid=student.sid
		)01课程成绩,
			(
			SELECT score from sc where cid='02' and sid=student.sid
		)02课程成绩
		from student 
	)temp where 01课程成绩>IFNULL(02课程成绩,0)
	;

-- 1.2 查询同时存在01和02课程的情况
	#1.关联
	SELECT student.*,sc1.score 01课程成绩,sc2.score 02课程成绩 from student
	inner join sc sc1 on  student.sid=sc1.sid and sc1.cid='01' 
	inner join sc sc2 on  student.sid=sc2.sid and sc2.cid='02' 
	where sc1.score>sc2.score
	;
	#2.子查询
	
	SELECT * from 
	(	
		SELECT *,
		(
			SELECT score from sc where cid='01' and sid=student.sid
		)01课程成绩,
			(
			SELECT score from sc where cid='02' and sid=student.sid
		)02课程成绩
		from student 
	)temp where 01课程成绩>02课程成绩
	;
	
-- 1.3 查询选择了02课程但没有01课程的情况
	SELECT * from 
	(	
		SELECT *,
		(
			SELECT score from sc where cid='01' and sid=student.sid
		)01课程成绩,
			(
			SELECT score from sc where cid='02' and sid=student.sid
		)02课程成绩
		from student 
	)temp where 02课程成绩 is not null and 01课程成绩 is null
	;


-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩
	  SELECT sid,
		(
			select sname from student where student.sid=sc.sid
		)学生姓名,ROUND(AVG(score),2)平均成绩
		from sc
		GROUP BY sid
		HAVING AVG(score)>=60;


-- 3.查询在 SC 表存在成绩的学生信息
	SELECT * from student where sid in 
	(
		SELECT DISTINCT sid from sc
  )

-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的成绩总和

SELECT *,
(
	SELECT count(*) from sc where sid=student.sid
)选课总数,
(
	SELECT sum(score) from sc where sid=student.sid
)成绩总和
from student


-- 5.查询「李」姓老师的数量

-- 6.查询学过「张三」老师授课的同学的信息

SELECT * from student where sid in 
(
	select sid from sc where cid in
	(
		SELECT cid from course where tid=
		(
			SELECT tid from teacher where tname='张三'
		)
	)
)
	
-- 7.查询没有学全所有课程的同学的信息

SELECT * from 
(
	SELECT *,
	(
		SELECT count(*) from sc where sid=student.sid
	)选课总数
	from student
)temp
where 选课总数<3



-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息
	
	SELECT DISTINCT student.* from student 
	inner join sc on sc.sid=student.sid
	where cid in 
	(
		select cid from sc where sid='01'
	) and student.sid !='01'
	

	

	


#沈国鑫




-- 9.查询和" 01 "号的同学学习的课程完全相同的其他同学的信息

-- 1.课程数量相同 01 =》3，
-- 2.至少有一门课与学号为" 01 "的同学所学相同
-- 3.查找学了01同学没学过的课程
-- 
-- 语文 数学 英语 体育
-- 
-- 01：语文 数学 
-- 
-- 02：数学 体育


SELECT student.*,sc1.cid ,sc2.cid ,sc3.cid from student 
LEFT join sc sc1 on  student.sid=sc1.sid and sc1.cid='01' 
LEFT join sc sc2 on  student.sid=sc2.sid and sc2.cid='02' 
LEFT join sc sc3 on  student.sid=sc3.sid and sc3.cid='03' 
where student.sid !='02'
and IFNULL(sc1.cid,0) =  IFNULL((SELECT cid from sc where sid='02' and cid='01'),0)
and IFNULL(sc2.cid,0) =  IFNULL((SELECT cid from sc where sid='02' and cid='02'),0)
and IFNULL(sc3.cid,0) =  IFNULL((SELECT cid from sc where sid='02' and cid='03'),0)


-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名
SELECT * from student where sid not in
(
	select sid from sc WHERE cid=
	(
		SELECT cid from course where tid=
		(
			SELECT tid from teacher where tname='张三'
		)
	)
)


-- 11.查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩
		
	select sid,sname,
	(
		SELECT ROUND(AVG(score),1) from sc where sc.sid =student.sid
	)平均成绩
	from student where sid in
	(
		SELECT sid from sc where score<60
		GROUP BY sid
		HAVING count(*)>=2
	)
		
-- 12.检索" 01 "课程分数小于 60，按分数降序排列的学生信息
	SELECT student.* from student 
	inner join 
	(
		SELECT * from sc 	WHERE cid='01' and score<60
	)temp on temp.sid=student.sid
	order by temp.score desc
	;


-- 13.按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩

	SELECT *,
	(
		SELECT score from sc where sc.sid=student.sid and cid='01'
	)语文,
	(
		SELECT score from sc where sc.sid=student.sid and cid='02'
	)数学,
	(
		SELECT score from sc where sc.sid=student.sid and cid='03'
	)英语 ,
	(
		SELECT AVG(score) from sc where sc.sid=student.sid 
	)平均分
  from student
	order by 平均分 desc
	


	




-- 14.查询各科成绩最高分、最低分和平均分,以如下形式显示：
-- 课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
-- 及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90
要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序
	####1.
	select *,
	(
		select max(score) from sc where sc.cid=course.cid
	)最高分,
	(
		select min(score) from sc where sc.cid=course.cid
	)最低分,
	(
		select avg(score) from sc where sc.cid=course.cid
	)平均分,
	(
		-- 及格人数
		( select count(score) from sc where sc.cid=course.cid and score>=60 )
		/
		-- 总人数	
		( select count(score) from sc where sc.cid=course.cid)
	)及格率,
	(
		-- 中等人数
		( select count(score) from sc where sc.cid=course.cid and score>=70 and score<80 )
		/
		-- 总人数	
		( select count(score) from sc where sc.cid=course.cid)
	)中等率,
	(
		-- 中等人数
		( select count(score) from sc where sc.cid=course.cid and score>=80 and score<90 )
		/
		-- 总人数	
		( select count(score) from sc where sc.cid=course.cid)
	)优良率,
	(
		-- 中等人数
		( select count(score) from sc where sc.cid=course.cid and score>=90  )
		/
		-- 总人数	
		( select count(score) from sc where sc.cid=course.cid)
	)优秀率
	from course;
	
	

########2.
	#最高分，最低分，平均分，及格率，中等率，优良率，优秀率
	SELECT cid,max(score) 最高分,min(score)最低分,avg(score)平均分,
	(
		count(IF(score>=60,score,null))/count(*)
	)及格率,
	(
		count(IF(score>=70 and score<80 ,score,null))/count(*)
	)中等率,
	(
		count(IF(score>=80 and score<90,score,null))/count(*)
	)优良率,
	(
		count(IF(score>=90,score,null))/count(*)
	)优秀率
	FROM sc
	GROUP BY cid;





-- 15.按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺
-- SQL SERVER  RANK()  
-- MYSQL 5.7 没有RANK  ;8.0才有RANK函数

-- 1关联
SELECT sc1.sid,sc1.cid,sc1.score,count(sc2.score)+1 名次 from sc sc1
left join sc sc2 on sc1.sid != sc2.sid and sc1.cid=sc2.cid and sc2.score>sc1.score
GROUP BY sc1.sid,sc1.cid,sc1.score
order by sc1.cid asc,名次 asc;


-- 2子查询
select *,(
	SELECT count(score)+1 from sc temp where temp.cid=sc.cid and temp.score>sc.score
)名次
from sc
order by cid asc,名次 asc;




-- 15.1 按各科成绩进行行排序，并显示排名， Score 重复时合并名次
SELECT sc1.sid,sc1.cid,sc1.score,count(DISTINCT sc2.score)+1 名次 from sc sc1
left join sc sc2 on sc1.sid != sc2.sid and sc1.cid=sc2.cid and sc2.score>sc1.score
GROUP BY sc1.sid,sc1.cid,sc1.score
order by sc1.cid asc,名次 asc;

select *,
(
	SELECT count(DISTINCT score)+1 from sc temp where temp.cid=sc.cid and temp.score>sc.score
)名次
from sc
order by cid asc,名次 asc;

-- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺

-- select *,-- (
-- 	select sum(score) from sc temp where temp.sid=student.sid 
-- )总成绩
-- from student


select *,
(
	SELECT count(总成绩)+1 from 
	(
		select *,
		(
			select IFNULL(sum(score),0) from sc temp where temp.sid=student.sid 
		)总成绩
		from student
	)	temp where temp.总成绩>t.总成绩
)名次
from 
	(
		select *,
		(
			select IFNULL(sum(score),0) from sc temp where temp.sid=student.sid 
		)总成绩
		from student
	) t
order by 总成绩 desc






-- 17. 统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比
	SELECT cid,
	(
		SELECT cname from course where course.cid=sc.cid
	)课程名称,
	(
		CONCAT(ROUND(100*count(IF(score>=85,1,null))/count(*),2),'%') 
	)`[100-85]`,
	(
		count(IF(score>=70 && score<85,1,null))/count(*)
	)`[85-70]`,
	(
		count(IF(score>=60 && score<70,1,null))/count(*)
	)`[70-60]`,
	(
		count(IF(score<60,1,null))/count(*)
	)`[60-0]`
	from sc
	GROUP BY cid




#张泊昌




-- mysql 8.0 才有rank 
-- 18.查询各科成绩前三名的记录
SELECT * from 
(
	select *,
	(
		SELECT count(score)+1 from sc temp where temp.cid=sc.cid and temp.score>sc.score
	)名次
	from sc
	order by cid asc,名次 asc
)temp 
where temp.名次<=3

-- 19.查询每门课程被选修的学生数
SELECT *,(
	SELECT count(*) from sc WHERE sc.cid=course.cid
)数量
from course


-- 20.查询出只选修两门课程的学生学号和姓名
SELECT * from student
where sid in
(
	SELECT sid from sc
	GROUP BY sid
	HAVING count(*)=2
)


-- 21. 查询男生、女生人数
SELECT 
(
	select count(*) from student where ssex='男'
)男生人数,
(
	select count(*) from student where ssex='女'
)女生人数

-- 22. 查询名字中含有「风」字的学生信息


-- 23查询同名同姓学生名单，并统计同名人数
select sname,count(*)人数 from student
GROUP BY sname
HAVING count(*)>1





-- 25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列
SELECT cid,avg(score)平均成绩 FROM sc
GROUP BY cid
ORDER BY 平均成绩 desc,cid asc


-- 26.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩
select * from student 
inner join 
(
	SELECT sid,avg(score)平均成绩 FROM sc
	GROUP BY sid
	HAVING avg(score)>=85
)temp
on temp.sid=student.sid




-- 27.查询课程名称为「数学」，且分数低于 60 的学生姓名和分数
	select * from student 
	inner join 
	(
			select * from sc
			where score<60
			and cid=
			(
				select cid from course where cname='数学'
			)
	)temp on temp.sid=student.sid


#林程铭





-- 28. 查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）

SELECT *,(
	SELECT score from sc where cid='01' and sid=student.sid
)语文,
(
	SELECT score from sc where cid='02' and sid=student.sid
)数学,
(
	SELECT score from sc where cid='03' and sid=student.sid
)英语
from student




-- 29.查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数

	SELECT 
	(
		select sname from student WHERE sid=sc.sid
	)姓名,
	(
		SELECT cname from course where cid=sc.cid
	)课程名称,
	sc.score 分数
	from sc 
	where score>70


-- 30.查询存在不及格同学的课程
select * from course WHERE cid in 
(
	SELECT DISTINCT cid from sc where score<60
)



-- 31.查询课程编号为 01 且课程成绩在 80 分以上的学生的学号和姓名
select * from student where sid in 
(
	SELECT sid FROM sc where score>80 and cid='01'
)


-- 32.求每门课程的学生人数
#1
select course.cid,course.cname,
(
	select count(*) from sc where cid=course.cid
)人数
from course
;
#2
SELECT course.cid,course.cname,temp.人数  from course
inner join 
(
	select cid,count(*)人数 from sc
	GROUP BY cid
)temp on temp.cid=course.cid
;


-- 33.成绩不重复，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩
#1
	select student.*,t.score from student
	inner join 
	(
			SELECT *
			from sc where cid =
			(
					select cid from course where tid=
					(
						SELECT tid from teacher where tname='张三'
					)
			)
			order by score desc
			LIMIT 1	
	)	t on t.sid=student.sid;
#2
		select * from student
		inner join sc on sc.sid=student.sid
		inner join course on course.cid=sc.cid
		inner join teacher on teacher.tid=course.tid
		where tname='张三'
		order by score desc
		LIMIT 1;

	
-- 34.成绩有重复的情况下，查询选修「张三」老师所授课程的学生中，成绩最高的学生


#1
	select student.*,t.score from student
	inner join 
	(
			SELECT *
			from sc where cid =
			(
					select cid from course where tid=
					(
						SELECT tid from teacher where tname='张三'
					)
			)
			and score>=
			(
				SELECT max(score) from sc where cid =
				(
						select cid from course where tid=
						(
							SELECT tid from teacher where tname='张三'
						)
				)
			)
	)	t on t.sid=student.sid

#2
	select student.*,sc.score from student
	inner join sc on sc.sid=student.sid
	inner join course on course.cid=sc.cid
	inner join teacher on teacher.tid=course.tid
	where tname='张三' and score>=
 (
		select max(score) from student
		inner join sc on sc.sid=student.sid
		inner join course on course.cid=sc.cid
		inner join teacher on teacher.tid=course.tid
		where tname='张三'
 )


-- 35.查询不同课程成绩相同的学生的学生编号、课程编号、学生成绩
#1
SELECT * from sc where sid in 
(
	select a.sid  from sc a
	inner join sc b on a.sid=b.sid and a.score=b.score and a.cid <>b.cid
) and score=
(
	select  max(a.score) from sc a
	inner join sc b on a.sid=b.sid and a.score=b.score and a.cid <>b.cid
)

#2
select a.sid,
(
	SELECT cname from course where cid=a.cid
)课程1,
(
	SELECT cname from course where cid=b.cid
)课程2,
a.score 分数  from sc a
inner join sc b on a.sid=b.sid and a.score=b.score and a.cid <>b.cid
where a.cid>b.cid




-- 36. 查询每门成绩最好的前两名

-- select cid,max(score) from sc
-- GROUP BY cid
-- 
-- -- 排除了第一名的所有人
-- 
-- 
-- t
-- 
-- select * from sc sc1 where 
-- score < 
-- (
-- 	select max(score) from sc sc2 where sc1.cid=sc2.cid
-- )
-- order by sc1.cid asc


#1 
select * from sc  where 
score>=
(
	select max(score) from 
	(
			select * from sc sc1 where 
			score < 
			(
				select max(score) from sc sc2 where sc1.cid=sc2.cid
			)
			order by sc1.cid asc
	)
	t  where t.cid=sc.cid
)
order by cid asc,score desc;

#2
SELECT * from 
(
	select *,
	(
		SELECT count(score)+1 from sc temp where temp.cid=sc.cid and temp.score>sc.score
	)名次
	from sc
	order by cid asc,名次 asc
)temp 
where temp.名次<=2;

-- 37. 统计每门课程的学生选修人数（超过 5 人的课程才统计）
	#1 
	SELECT cid,count(*)人数 from sc
	GROUP BY cid
	HAVING count(*)>5;
	
	#2
	select * from 
	(
		select *,
		(
			select COUNT(*) from sc where sc.cid=course.cid
		)人数	from course
	)temp where temp.人数>5;


#苏灿莹



-- 38.检索至少选修两门课程的学生学号
		select sid from sc
		GROUP BY sid
		HAVING count(*)>=2
	



-- 39.查询选修了全部课程的学生信息
	select * from student where sid in
	(
		select sid from sc
		GROUP BY sid
		HAVING count(*)>=
		(
			select count(*) from course
		)
	)





-- 24.查询 1990 年出生的学生名单
	#1
	select * from student where sage>='1990' and sage<'1991';
  #2
  select * from student where YEAR(sage)='1990';
	


-- 40.查询各学生的年龄，只按年份来算

 select *,YEAR(now())-YEAR(sage) as 年龄 from student ;


-- 41. 按照出生日期来算，当前月日 < 出生年月的月日则，年龄减一
-- if(当前月份>出生月份)
-- {
-- 	YEAR(now())-YEAR(sage) 
-- }else if(当前月份=出生月份){
-- 	if(当前日期>=出生日期)
-- 	{
-- 		YEAR(now())-YEAR(sage) 
-- 	}else{
-- 		YEAR(now())-YEAR(sage) -1
-- 	}
-- }else{//当前月份<出生月份
-- 
-- 	YEAR(now())-YEAR(sage) -1
-- }
#1.
 select *,
 ( 
		CASE 
		WHEN MONTH(NOW())>MONTH(sage) THEN #当前月份>出生月份
			YEAR(now())-YEAR(sage) 
		when  MONTH(NOW())=MONTH(sage) THEN
			CASE 
	    WHEN DAYOFMONTH(NOW())>=DAYOFMONTH(sage) THEN
		    YEAR(now())-YEAR(sage) 
	     ELSE
		     YEAR(now())-YEAR(sage) -1
      END
		ELSE #当前月份<出生月份
			YEAR(now())-YEAR(sage) -1
		END
 )周岁 
 from student;

#2.
select *,(
 TIMESTAMPDIFF(YEAR,sage,now())
)周岁
from student




-- 42.查询本周过生日的学生


--  	select * from student where WEEKOFYEAR(NOW())=WEEKOFYEAR(sage)
-- 	
-- 	select WEEKOFYEAR('1990-05-20'),WEEKOFYEAR('1991-05-20'),WEEKOFYEAR('1992-05-20'),WEEKOFYEAR('1993-05-20')
-- 	
	select * from student where WEEKOFYEAR(NOW())=WEEKOFYEAR(ADDDATE(sage,INTERVAL (YEAR(now())-YEAR(sage)) YEAR)) 
	
1990+(2021-1990)
	
-- 43. 查询下周过生日的学生
	select * from student where (WEEKOFYEAR(NOW())+1)%53=WEEKOFYEAR(ADDDATE(sage,INTERVAL (YEAR(now())-YEAR(sage)) YEAR)) 
	

-- 44.查询本月过生日的学生
	 select * from student where MONTH(now())=MONTH(sage)

-- 45.查询下月过生日的学生
	select * from student where MONTH(sage)=(MONTH(now())+1)%12

